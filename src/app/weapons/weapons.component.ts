import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';

@Component({
  selector: 'app-weapons',
  templateUrl: './weapons.component.html',
  styleUrls: ['./weapons.component.css']
})
export class WeaponsComponent implements OnInit {
  items = LIB.get('weapons');
  selectedId = 'FACT-ADAS';

  displayedColumns: string[] = ['name', 'reach', 'type', 'f', 'pa', 'd', 'cost', 'aptitude'];

  constructor() { }

  ngOnInit() {
  }

}
