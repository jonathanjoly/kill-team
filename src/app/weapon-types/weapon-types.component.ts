import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';

@Component({
  selector: 'app-weapon-types',
  templateUrl: './weapon-types.component.html',
  styleUrls: ['./weapon-types.component.css']
})
export class WeaponTypesComponent implements OnInit {
  items = LIB.get('weapons-type');
  selectedId = 'FACT-ADAS';
  constructor() { }

  ngOnInit() {
  }

}
