import { WeaponsComponent } from './weapons/weapons.component';
import { TeamsComponent } from './teams/teams.component';
import { MissionsComponent } from './missions/missions.component';
import { HomeComponent } from './home/home.component';
import { HistoricsComponent } from './historics/historics.component';
import { EquipementsComponent } from './equipements/equipements.component';
import { BehaviorsComponent } from './behaviors/behaviors.component';
import { AptitudesComponent } from './aptitudes/aptitudes.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SingularitiesComponent } from './singularities/singularities.component';
import { SpecialitiesComponent } from './specialities/specialities.component';
import { UnitsComponent } from './units/units.component';




const routes: Routes = [
  { path: 'aptitudes', component: AptitudesComponent },
  { path: 'behaviors', component: BehaviorsComponent },
  { path: 'equipements', component: EquipementsComponent },
  { path: 'historics', component: HistoricsComponent },
  { path: 'home', component: HomeComponent },
  { path: 'missions', component: MissionsComponent },
  { path: 'singularities', component: SingularitiesComponent },
  { path: 'specialities', component: SpecialitiesComponent },
  { path: 'teams', component: TeamsComponent },
  { path: 'units', component: UnitsComponent },
  { path: 'weapons', component: WeaponsComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];


@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule {}
