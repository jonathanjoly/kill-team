import LIB from '../../../killteam-data/app';

function memberPoint(member) {
  return getCost(member.unit) + getSum(member.weapons) + getSum(member.options);
}
function teamPoint(team) {
  return team.members.reduce(teamReducer, 0);
}

function teamReducer(sum, member) {
  return sum + memberPoint(member);
}
function getSum(items) {
  return items.reduce(sumReducer, 0);
}

function sumReducer(sum, id) {
  return sum + getCost(id);
}

function getCost(id) {
  return LIB.get(id).cost;
}

export {
  memberPoint,
  teamPoint
};
