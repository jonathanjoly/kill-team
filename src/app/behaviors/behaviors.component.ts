import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';

@Component({
  selector: 'app-behaviors',
  templateUrl: './behaviors.component.html',
  styleUrls: ['./behaviors.component.css']
})
export class BehaviorsComponent implements OnInit {
  items = LIB.get('behaviors');
  selectedId = 'FACT-ADAS';
  constructor() { }

  ngOnInit() {
  }

}
