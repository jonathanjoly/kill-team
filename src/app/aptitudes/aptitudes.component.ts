import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';

@Component({
  selector: 'app-aptitudes',
  templateUrl: './aptitudes.component.html',
  styleUrls: ['./aptitudes.component.css']
})
export class AptitudesComponent implements OnInit {
  items = LIB.get('aptitudes');
  selectedId = 'FACT-ADAS';
  constructor() { }

  ngOnInit() {
  }

}
