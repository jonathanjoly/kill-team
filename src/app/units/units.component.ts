import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.css']
})
export class UnitsComponent implements OnInit {
  items = LIB.get('units');
  selectedId = 'FACT-ADME';
  constructor() { }

  aptitudeName(aptitude) {
    return LIB.get(aptitude).name;
  }

  aptitudeDescription(aptitude) {
    return LIB.get(aptitude).description;
  }
  ngOnInit() {
  }

}
