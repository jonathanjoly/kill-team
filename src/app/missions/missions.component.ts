import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';

@Component({
  selector: 'app-missions',
  templateUrl: './missions.component.html',
  styleUrls: ['./missions.component.css']
})
export class MissionsComponent implements OnInit {
  items = LIB.get('missions');
  selectedId = 'FACT-ADAS';
  constructor() { }

  ngOnInit() {
  }

}
