import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingularitiesComponent } from './singularities.component';

describe('SingularitiesComponent', () => {
  let component: SingularitiesComponent;
  let fixture: ComponentFixture<SingularitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingularitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingularitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
