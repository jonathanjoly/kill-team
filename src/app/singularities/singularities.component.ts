import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';

@Component({
  selector: 'app-singularities',
  templateUrl: './singularities.component.html',
  styleUrls: ['./singularities.component.css']
})
export class SingularitiesComponent implements OnInit {
  items = LIB.get('singularities');
  selectedId = 'FACT-ADAS';
  constructor() { }

  ngOnInit() {
  }

}
