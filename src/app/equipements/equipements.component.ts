import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';
@Component({
  selector: 'app-equipements',
  templateUrl: './equipements.component.html',
  styleUrls: ['./equipements.component.css']
})
export class EquipementsComponent implements OnInit {
  items = LIB.get('equipements');
  selectedId = 'FACT-ADAS';
  constructor() { }

  ngOnInit() {
  }

}
