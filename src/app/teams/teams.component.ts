import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';
import {teamPoint} from '../computer';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {
  teams = LIB.teams.items;
  selection = LIB.teams.items[0];

  constructor() { }

  onSelect(id): void {
    this.selection = id;
  }

  getName(name) {
    try {return LIB.get(name).name; } catch {
    // console.log('Not found', name);
     // console.log('get value', LIB.get(name));
      return '';
    }
  }

  getPoint(team) {
    return teamPoint(team);
  }

  ngOnInit() {
  }

}
