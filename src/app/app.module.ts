import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {MaterialModule} from '../material-module';
import { AppComponent } from './app.component';
import { AptitudesComponent } from './aptitudes/aptitudes.component';
import { BehaviorsComponent } from './behaviors/behaviors.component';
import { EquipementsComponent } from './equipements/equipements.component';
import { HistoricsComponent } from './historics/historics.component';
import { MissionsComponent } from './missions/missions.component';
import { SingularitiesComponent } from './singularities/singularities.component';
import { SpecialitiesComponent } from './specialities/specialities.component';
import { TeamsComponent } from './teams/teams.component';
import { WeaponsComponent } from './weapons/weapons.component';
import { WeaponTypesComponent } from './weapon-types/weapon-types.component';
import { FactionFilterComponent } from './faction-filter/faction-filter.component';
import { MenuComponent } from './menu/menu.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './home/home.component';
import { UnitsComponent } from './units/units.component';

import LIB from '../../../killteam-data/app';
import { MemberComponent } from './member/member.component';
// console.log(LIB.teams);
console.log(LIB.teams.items[0].members[0]);
@NgModule({
  declarations: [
    AppComponent,
    AptitudesComponent,
    BehaviorsComponent,
    EquipementsComponent,
    HistoricsComponent,
    MissionsComponent,
    SingularitiesComponent,
    SpecialitiesComponent,
    TeamsComponent,
    WeaponsComponent,
    WeaponTypesComponent,
    FactionFilterComponent,
    MenuComponent,
    HomeComponent,
    UnitsComponent,
    MemberComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
