import { Component, OnInit, OnChanges, Input } from '@angular/core';
import LIB from '../../../../killteam-data/app';
import {memberPoint} from '../computer';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnChanges, OnInit {

  @Input() member;
  unit;

  getName(name) {
    try {return LIB.get(name).name; } catch {
     console.log('Not found', name);
     // console.log('get value', LIB.get(name));
      return '';
    }
  }

  getProfileField(name, field) {
    try {return LIB.get(name).profile[field]; } catch {
     console.log('Not found', name);
     // console.log('get value', LIB.get(name));
      return '';
    }

  }
  getField(name, field) {
    try {return LIB.get(name)[field]; } catch {
     console.log('Not found', name);
     // console.log('get value', LIB.get(name));
      return '';
    }

  }

  getPoint(member) {
    return memberPoint(member);
  }


  ngOnChanges(changes ) {
    // this.unit = LIB.get(changes.member.unit);
  }

  constructor() { }

  ngOnInit() {

  }

}
