import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import LIB from '../../../../killteam-data/app';

@Component({
  selector: 'app-faction-filter',
  templateUrl: './faction-filter.component.html',
  styleUrls: ['./faction-filter.component.css']
})
export class FactionFilterComponent implements OnInit {
  factions = LIB.get('factions');
  @Input() selection;
  @Output() selectionChange = new EventEmitter();

  onSelect(id): void {
    this.selection = id;
    this.selectionChange.emit(this.selection);
  }
  constructor() { }

  ngOnInit() {
  }

}
