import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FactionFilterComponent } from './faction-filter.component';

describe('FactionFilterComponent', () => {
  let component: FactionFilterComponent;
  let fixture: ComponentFixture<FactionFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactionFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FactionFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
