import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';
@Component({
  selector: 'app-historics',
  templateUrl: './historics.component.html',
  styleUrls: ['./historics.component.css']
})
export class HistoricsComponent implements OnInit {
  items = LIB.get('historics');
  selectedId = 'FACT-ADAS';
  constructor() { }

  ngOnInit() {
  }

}
