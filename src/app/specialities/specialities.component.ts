import { Component, OnInit } from '@angular/core';
import LIB from '../../../../killteam-data/app';

@Component({
  selector: 'app-specialities',
  templateUrl: './specialities.component.html',
  styleUrls: ['./specialities.component.css']
})
export class SpecialitiesComponent implements OnInit {
  items = LIB.get('specialities');
  constructor() { }

  ngOnInit() {
  }

}
